+++
title = "Guide"
slug = "guide"
+++

See medium for user guide:

- [How to use BSC with MetaMask](/guidesub/guidesub/)

- Chives Swap, a web wallet for both Chives and Chia

- How to wrap XCC into BSC chain token PXCC

- How to convert PXCC back to XCC

- How to buy/sell PXCC with Pancakeswap

- Provide liquidity and get profit

- How to obtain more Chives? A way to arbitrate with PlotBridge
