+++
title = "Guide"
slug = "guide"
+++

用户指南:

-   [如何在币安智能链上使用 MetaMask(小狐狸钱包)](/zh-cn/guidesub/guidesub/)

-   Chives Swap，一个同时适用于 Chives(韭菜) 和 Chia 的轻钱包

-   如何将 XCC 转换成 BSC 链上的代币 PXCC

-   如何将 PXCC 转换回 XCC

-   如何用 Pancakeswap 购买/出售 PXCC

-   提供流动性并获得收益

-   如何获得更多的 XCC? 使用 PlotBridge 去套利
