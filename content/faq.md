+++
title = "FAQ"
aliases = ["faq"]
+++

## Common
#### Q: Why do you choose Binance Smart Chain (BSC)?
A: IMHO, BSC is the second popular smart chain for DeFi application. Ethereum is the most popular chain, but the transaction fee is always so high! So, a balanced choice is BSC. We may also support more smart chain in the future, perhaps MATICS? Do you have a suggestion?


## Usage
#### Q: How long will it take to receive my PXCC/PXCH token?
A: After XCH/XCC arrive the deposit address, it usually takes less than 10 minutes to receive your PXCH/PXCC token. If more than 10 minutes passed and the token still __NOT__ arrive, please contact our team in Discord or Telegram official channel.



